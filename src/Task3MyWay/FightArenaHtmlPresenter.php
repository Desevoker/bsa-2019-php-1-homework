<?php

declare(strict_types=1);

namespace App\Task3MyWay;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    protected $arena;
    protected $presentation;

    public function __construct(FightArena $arena)
    {
        $this->arena = $arena;
    }

    public function present(): string
    {
        $this->preparePresentation();
        return $this->presentation;
    }

    protected function preparePresentation()
    {
        $titleText = "There are no fighters yet.";
        $fightersHtml = "";

        if (count($this->arena->all()) > 0) {
            $titleText = "Today on the arena:";

            foreach ($this->arena->all() as $fighter) {
                $fightersHtml .= "<div class=\"col-4\">
                    <div class=\"card border-dark\">
                        <img src=\"{$fighter->getImage()}\" class=\"card-img-top py-2 px-5\" style=\"max-height: 350px; width: auto;\" alt=\"{$fighter->getName()} image\">
                        <div class=\"card-body\">
                            <h5 class=\"card-title\">{$fighter->getName()}</h5>
                        </div>
                        <ul class=\"list-group list-group-flush\">
                            <li class=\"list-group-item\">Health: {$fighter->getHealth()}</li>
                            <li class=\"list-group-item\">Attack: {$fighter->getAttack()}</li>
                        </ul>
                    </div>
                </div>";
            }
        }

        $arenaHtml = "<div class=\"row\"><div class=\"col-12\">
            <div class=\"card border-dark text-center my-4\">
                <h5 class=\"card-header text-white bg-dark\">Fight Arena</h5>
                <div class=\"card-body\">
                    <h5 class=\"card-title\">{$titleText}</h5>
                    <div class=\"row\">
                        {$fightersHtml}
                    </div>
                </div>
                <ul class=\"list-group list-group-flush\">
                    <li class=\"list-group-item text-white bg-danger\">Most powerfull fighter: {$this->arena->mostPowerful()->getName()}</li>
                    <li class=\"list-group-item text-white bg-success\">Most healthy fighter: {$this->arena->mostHealthy()->getName()}</li>
                </ul>
            </div>
        </div></div>";

        $this->presentation = $arenaHtml;
    }
}
