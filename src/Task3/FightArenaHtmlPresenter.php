<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    protected $arena;
    protected $presentation;

    public function __construct(FightArena $arena)
    {
        $this->arena = $arena;
    }

    public function present(): string
    {
        $this->preparePresentation();
        return $this->presentation;
    }

    protected function preparePresentation()
    {
        if (count($this->arena->all()) > 0) {
            foreach ($this->arena->all() as $fighter) {
                $this->presentation .= "<img src=\"{$fighter->getImage()}\"><p>{$fighter->getName()}: {$fighter->getHealth()}, {$fighter->getAttack()}</p>";
            }
        }
    }
}
