<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    protected $fighters = [];

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        if (count($this->fighters) === 1) {
            return $this->fighters[0];
        }

        $maxAttack = 0;
        $onFighter;

        foreach ($this->fighters as $fighter) {
            if ($fighter->getAttack() > $maxAttack) {
                $maxAttack = $fighter->getAttack();
                $onFighter = $fighter;
            }
        }

        return $onFighter;
    }

    public function mostHealthy(): Fighter
    {
        if (count($this->fighters) === 1) {
            return $this->fighters[0];
        }

        $maxHealth = 0;
        $onFighter;

        foreach ($this->fighters as $fighter) {
            if ($fighter->getHealth() > $maxHealth) {
                $maxHealth = $fighter->getHealth();
                $onFighter = $fighter;
            }
        }

        return $onFighter;
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
